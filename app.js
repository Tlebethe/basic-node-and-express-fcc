

let express = require('express');
let app = express();

console.log('Hello World')

//import body parser
let bodyParser = require('body-parser')

//serve hello express

// app.get('/', (req,res) => {
//   res.send('Hello Express')
// })

//Implement a Root-Level Request Logger Middleware
app.get('',(req, res, next)=> {
  method = req.method
  path = req.path
  ip = req.ip
  console.log(`${method} ${path} - ${ip}`)
  next()
})

//Use body-parser to Parse POST Requests
app.use(bodyParser.urlencoded({extended: false}))

//serve static assets
app.use('/public', express.static(__dirname+'/public'))

//serve an html file
app.get('/', (req,res)=>{
  path= __dirname+'/views/index.html'
  res.sendFile(path)
})

//serve json on a specific route
app.get('/json', function(req, res){
  //using the env 
  protocol = process.env.MESSAGE_STYLE
  if (protocol === 'uppercase' ){
    console.log('oops')
    res.json({message: "Hello json".toUpperCase()})
  }else {
    res.json({message: "Hello json"})
  }
  
})

//chain middleware to create a time server
app.get('/now', (req, res, next)=>{
  req.time = new Date().toString()
  res.json({time: req.time})
})


//Get Route Parameter Input from the Client
app.get('/:word/echo', (req, res)=>{
  let word = req.params.word
  res.json({word: word})
})

//Get Query Parameter Input from the Client
app.get('/name', (req, res)=> {
  let firstname = req.query.first
  let lastname = req.query.last
  res.json({name: `${firstname} ${lastname}`})
})

//get data from a post request
app.post('/name', (req, res)=> {
  let firstname = req.body.first
  let lastname = req.body.last
  console.log(`${firstname} ${lastname}`)
  res.json({name: `${firstname} ${lastname}`})
})









 module.exports = app;
